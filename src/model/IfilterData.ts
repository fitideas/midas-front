import { format } from 'date-fns';
import { environment } from '../environments/environment';

export class FilterData {
  tipoDocumento: number | null;
  numeroDocumento: number | null;
  numeroSuministro: number | null;
  numeroContrato: number | null;
  SubProducto: number | null;
  fechaInicial: string | null;
  fechaFinal: string | null;
  typeQuery: number | null;
  ext: number | null;
  limit: number;
  offset: number;
  numeroServicio: number | null;
  verMas: boolean | null; 

  constructor(
    tipoDocumento: number | null = null,
    numeroDocumento: number | null = null,
    numeroSuministro: number | null = null,
    numeroContrato: number | null = null,
    SubProducto: number | null = null,
    fechaInicial: Date | null = null,
    fechaFinal: Date | null = null,
    numeroServicio: number | null = null,
    typeQuery: number | null = null,
    ext: number | null = null,    
    verMas: boolean | null = null,
  ) {
    this.tipoDocumento = tipoDocumento;
    this.numeroDocumento = numeroDocumento;
    this.numeroSuministro = numeroSuministro;
    this.numeroContrato = numeroContrato;
    this.SubProducto = SubProducto;
    this.typeQuery = typeQuery;
    this.ext = ext;
    this.fechaInicial = fechaInicial != null ? format(fechaInicial, 'yyyy-MM-dd') : null;
    this.fechaFinal = fechaFinal != null ? format(fechaFinal, 'yyyy-MM-dd') : null;
    this.limit = environment.pageSize;    
    this.offset = 0;
    this.numeroServicio = numeroServicio;
    this.verMas = verMas;
  }
}
