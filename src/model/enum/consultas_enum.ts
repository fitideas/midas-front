export enum ConsultaEnum {
    ClienteHistorico =1,
    Cliente = 2,
    ClienteRefinanciaciones = 3,
    ClienteCondonaciones = 4,
    ClienteFacturacion = 5,
    ClienteRecaudo = 6,
    ClienteCambioEstado = 7,
    ClienteTraslados = 8
  }