import { ThemeProvider } from '@mui/material/styles';
import './App.css';
import ResponsiveAppBar from './components/appbar';
import { Footer } from './components/footer';
import AppRoutes from './routes';
import mainTheme from './theme/theme';

function App() {
  return (
    // <ThemeProvider theme={mainTheme}>
    <ThemeProvider theme={mainTheme}>
      <>
          <ResponsiveAppBar></ResponsiveAppBar>
          <AppRoutes></AppRoutes>
          <Footer></Footer>
      </>
    </ThemeProvider>
  );
}

export default App;
