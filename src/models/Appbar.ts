/**
 * Menu Model Object
 */
export interface Menu {
    /**
     * Label of the menu
     */
    name: string,

    /**
     * Path to navigate using the menu
     */
    path?: string,

    /**
     * Callback for execute using the menu
     */
    callback?: Function,

    /**
     * Array of SubMenus
     */
    submenu?: Menu[]
}
