export interface SelectButtonProps {
    options: SelectButtonOptions[];
}

export interface SelectButtonOptions {
    label: JSX.Element,
    value: string,
    callback: () => void
}