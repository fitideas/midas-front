/**
 * AppBar definition
 */

// React
import * as React from 'react';

// Material Design
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import Container from '@mui/material/Container';
import Avatar from '@mui/material/Avatar';
import Tooltip from '@mui/material/Tooltip';
import MenuItem from '@mui/material/MenuItem';
import Badge from '@mui/material/Badge';
import NotificationsIcon from '@mui/icons-material/Notifications';
import HelpIcon from '@mui/icons-material/Help';
import NestedMenuItem from "../../lib/material-ui-nested-menu-item";
import "./style.css";
// Router
import {NavigateFunction, useNavigate} from 'react-router-dom';

// Midas
import logo from '../../assets/logo.png';
import {Menu as MenuModel} from '../../models/Appbar'
import {Button} from "@mui/material";

/**
 * Settings in Profile Menu
 */
const settings: string[] = ['Utima conexion: 01/01/01', 'Cerrar sesion'];

/**
 * Menu object
 */
const menus: MenuModel[] = [
    {
        name: "CONSULTAS",
        path: "",
        submenu: [
            {
                name: "Cliente",
                path: "/consultas"
            },
            {
                name: "Consulta 2",
                path: "",
                submenu: [
                    {
                        name: "Consulta 3",
                        path: ""
                    }
                ]
            }
            
        ]
    },
    {
        name: "REPORTES",
        path: "",
        submenu: [
            {
                name: "CONSULTA 2",
                path: ""
            }
        ]
    },
    {
        name: "CARGUES",
        path: "",
        submenu: [
            {
                name: "Cargar gestión de cobro",
                path: "/cargueGestionCobro"
            }
        ]
    },
    {
        name: "CARTERA",
        path: "",
        submenu: [
            {
                name: "Cargues",
                path: "",
                submenu: [
                    {
                        name: "Calendario de facturación",
                        path: "/calendarioFacturacion",
                    },
                    {
                        name: "Información de reclamaciones en curso",
                        path: "cargueReclamaciones",
                    },
                    {
                        name: "Generar información de cartera para casas de cobranza",
                        path: "/casasDeCobranza"
                    }
                ]
            },
            {
                name: "Procesos",
                path: "",
                submenu: [
                    {
                        name: "Generar Carta a morosos",
                        path: "/generarCartasMorosos",
                    },
                    {
                        name: "Generar segundo preaviso",
                        path: "/generarSegundoPreaviso",
                    },
                    {
                        name: "Eliminar Cartas a morosos",
                        path: "/eliminarCartasMorosos",
                    }
                ]
            },
            {
                name: "Afectos Gastos de Cobranza",
                path: "/AfectosGastosCobranza",
                submenu: []
            }
        ]
    }
];

/**
 * Construct the AppBar
 * @constructor
 */
function ResponsiveAppBar(): JSX.Element {

    let navigator: NavigateFunction = useNavigate();


    /**
     * Element of the Logo component of the App Bar
     */
    const useLogoComponent = (): JSX.Element => {
        return (
            <Box sx={{flexGrow: 0}}>
                <img src={logo} alt=""/>
                <Typography align="center" variant="h6" noWrap component="div">
                    MIDAS
                </Typography>
            </Box>
        );
    }

    /**
     * Menu App Bar Component
     */
    const useMenuComponent = (): JSX.Element => {

        /**
         * Menu Visibility State
         */
        const [menuVisibilities, setMenuVisibilities] = React.useState<Array<boolean>>([]);
        const [menuAnchors, setMenuAnchors] = React.useState<Array<null | HTMLElement>>([]);

        /**
         * Open Menu on click
         * @param event Mouse Event
         * @param index Index por Menu
         */
        const handleOpenMenu = (event: React.MouseEvent<HTMLElement>, index: number): void => {
            let currentMenuVisibilities = menuVisibilities.slice();
            currentMenuVisibilities[index] = true;
            setMenuVisibilities(currentMenuVisibilities);

            let currentMenuAnchors = menuAnchors.slice();
            currentMenuAnchors[index] = event.currentTarget;
            setMenuAnchors(currentMenuAnchors);

        };

        /**
         * Close Menu
         */
        const handleCloseMenu = (index: number): void => {
            let currentMenuVisibilities = menuVisibilities.slice();
            currentMenuVisibilities[index] = false;
            setMenuVisibilities(currentMenuVisibilities);

            let currentMenuAnchors = menuAnchors.slice();
            currentMenuAnchors[index] = null;
            setMenuAnchors(currentMenuAnchors);
        };


        const changeRoute = (path: string): void => {
            if(path !== ""){
                navigator(path)
            }
        }


        /**
         * Construct a subMenu from MenuModel
         * @param menu Menu Model
         * @param index Menu Index
         */
        const subMenu = (menu: MenuModel, index: number): JSX.Element => {
            if (menu.submenu instanceof Array && menu.submenu.length > 0) {
                let content: JSX.Element[] = [];
                if (menu.submenu !== []) {
                    for (let i = 0; i < menu.submenu.length; i++) {
                        content.push(subMenu(menu.submenu[i], index));
                    }
                }
                return (
                    <NestedMenuItem parentMenuOpen={menuVisibilities[index]} label={menu.name}>
                        {content}
                    </NestedMenuItem>
                );
            } else {
                return (
                    <MenuItem onClick={
                        () => {
                            if(menu.path != null)
                                changeRoute(menu.path);
                            if(menu.callback != null)
                                menu.callback();
                            handleCloseMenu(index)}}>
                        {menu.name}
                    </MenuItem>
                )
            }
        }

        /**
         * Construct a button from MenuModel
         * @param menu Menu Model
         * @param index Menu Index
         */
        const dropdownMenu = (menu: MenuModel, index: number): JSX.Element => {
            let content: JSX.Element[] = [];
            if (menu.submenu instanceof Array && menu.submenu.length > 0) {
                for (let i = 0; i < menu.submenu.length; i++) {
                    content.push(subMenu(menu.submenu[i], index));
                }
            }
            if(content.length <= 0){
                return (
                    <div className='box-menu'>
                        <Button onClick={() => {if(menu.path != null) changeRoute(menu.path)}} sx={{my: 1, color: 'white', width:"100%"}}>
                            {menu.name}
                        </Button>
                    </div>
                )
            } else {
                return (
                    <div className='box-menu'>
                        <Button onClick={(event: React.MouseEvent<HTMLElement>) => {handleOpenMenu(event, index)}} sx={{my: 1, color: 'white', width:"100%"}}>
                            {menu.name}
                        </Button>
                        <Menu
                            sx={{mt: '45px'}}
                            id="menu-appbar"
                            anchorEl={menuAnchors[index]}
                            anchorOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            keepMounted
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            open={menuVisibilities[index]}
                            onClose={() => {handleCloseMenu(index)}}
                        >
                            {content}
                        </Menu>
                    </div>
                )
            }

        }

        return (
            <Box sx={{flexGrow: 1, display: 'flex', justifyContent: 'center', p: '1', m: '1'}}>
                {
                    menus.map((value: MenuModel, index: number) => (
                        dropdownMenu(value, index)
                    ))
                }
            </Box>
        );
    }

    /**
     * Profile App Bar Component
     */
    const useProfileComponent = (): JSX.Element => {

        /**
         * Menu Visibility State
         */
        const [menuVisibility, setMenuVisibility] = React.useState <null | HTMLElement>(null);

        /**
         * Open Menu on click
         * @param event Click Event
         */
        const handleOpenMenu = (event: React.MouseEvent<HTMLElement>): void => {
            setMenuVisibility(event.currentTarget);
        };

        /**
         * Close Menu
         */
        const handleCloseMenu = (): void => {
            setMenuVisibility(null);
        };

        /**
         * Function to construct the menu from settings array
         */
        const getOptions = (): JSX.Element[] => {
            return settings.map((setting) => (
                <MenuItem key={setting} onClick={handleCloseMenu}>
                    <Typography textAlign="center">{setting}</Typography>
                </MenuItem>
            ));
        }

        return (
            <Box sx={{flexGrow: 0}}>
                <IconButton size="large" aria-label="show 17 new notifications" color="inherit">
                    <Badge badgeContent={17} color="error">
                        <NotificationsIcon fontSize="inherit"/>
                    </Badge>
                </IconButton>
                <IconButton size="large" aria-label="show 17 new notifications" color="inherit">
                    <HelpIcon fontSize="inherit"/>
                </IconButton>
                <Tooltip title="Rodrigo Alvarez">
                    <IconButton onClick={handleOpenMenu} sx={{p: 0, paddingLeft: '10px'}}>
                        <Avatar alt="Remy Sharp" src="/static/images/avatar/2.jpg"/>
                    </IconButton>
                </Tooltip>
                <Menu
                    sx={{mt: '45px'}}
                    id="menu-appbar"
                    anchorEl={menuVisibility}
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                    }}
                    keepMounted
                    transformOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                    }}
                    open={menuVisibility != null}
                    onClose={handleCloseMenu}
                >
                    {getOptions()}
                </Menu>
            </Box>
        )
    }

    return (
        <AppBar style={{position: 'sticky', top: '0'}} position="static">
            <div id="menuRef"></div>
            <Container maxWidth="xl">
                <Toolbar disableGutters>
                    {useLogoComponent()}
                    {useMenuComponent()}
                    {useProfileComponent()}
                </Toolbar>
            </Container>
        </AppBar>
    );
}

export default ResponsiveAppBar;
