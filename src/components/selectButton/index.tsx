/**
 * Select button component
 */

// React
import React, {MutableRefObject} from "react";

// Material Design
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import ClickAwayListener from '@mui/material/ClickAwayListener';
import Grow from '@mui/material/Grow';
import Paper from '@mui/material/Paper';
import Popper from '@mui/material/Popper';
import MenuItem from '@mui/material/MenuItem';
import MenuList from '@mui/material/MenuList';

// Midas
import {SelectButtonOptions, SelectButtonProps} from "../../models/SelectButtonOption";


/**
 * Select Button Component
 * @param props Component props
 * @constructor
 */
export default function SelectButton(props: SelectButtonProps){

    /**
     * Open React State
     */
    const [open, setOpen]: [boolean, React.Dispatch<React.SetStateAction<boolean>>] = React.useState<boolean>(false);

    /**
     * Selected Index State
     */
    const [selected, setSelected]: [SelectButtonOptions, React.Dispatch<React.SetStateAction<SelectButtonOptions>>] = React.useState<SelectButtonOptions>(props.options[0]);

    /**
     * Anchor Reference
     */
    const anchorRef: MutableRefObject<HTMLDivElement | null> = React.useRef<HTMLDivElement | null>(null);

    /**
     * Button Values
     */
    const buttonValues: SelectButtonOptions[] = props.options;

    /**
     * Menu Item Click Event handler
     * @param event
     * @param index
     */
    const handleMenuItemClick = (event: React.MouseEvent<HTMLLIElement, MouseEvent>, option: SelectButtonOptions): void => {
        setSelected(option);
        setOpen(false);
    };

    /**
     * HandleClick
     */
    const handleClick = (): void => {
        selected.callback();
    }

    /**
     * Handle Toggle Callback
     */
    const handleToggle = (): void => {
        setOpen((prevOpen) => !prevOpen);
    };

    /**
     * Handle Close Callback
     * @param event Event Object
     */
    const handleClose = (event: Event) => {
        if (anchorRef.current != null && anchorRef.current.contains(event.target as HTMLElement)) {
            return;
        }
        setOpen(false);
    };

    return (
        <React.Fragment>
            <ButtonGroup variant="contained" ref={anchorRef} aria-label="split button">
                <Button onClick={handleClick}>{selected.label}</Button>
                <Button
                    size="small"
                    aria-controls={open ? 'split-button-menu' : undefined}
                    aria-expanded={open ? 'true' : undefined}
                    aria-label="select merge strategy"
                    aria-haspopup="menu"
                    onClick={handleToggle}
                >
                    <ArrowDropDownIcon />
                </Button>
            </ButtonGroup>
            <Popper
                open={open}
                anchorEl={anchorRef.current}
                role={undefined}
                transition
                disablePortal
            >
                {({ TransitionProps, placement }) => (
                    <Grow
                        {...TransitionProps}
                        style={{
                            transformOrigin:
                                placement === 'bottom' ? 'center top' : 'center bottom',
                        }}
                    >
                        <Paper>
                            <ClickAwayListener onClickAway={handleClose}>
                                <MenuList id="split-button-menu" autoFocusItem>
                                    {buttonValues.map((option: SelectButtonOptions) => (
                                        <MenuItem
                                            selected={selected == option}
                                            onClick={(event) => handleMenuItemClick(event, option)}
                                        >
                                            {option.label}
                                        </MenuItem>
                                    ))}
                                </MenuList>
                            </ClickAwayListener>
                        </Paper>
                    </Grow>
                )}
            </Popper>
        </React.Fragment>
    );
}