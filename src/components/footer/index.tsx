import * as React from 'react';
import "./index.css";


export class Footer extends React.Component {
  render() {
    return (
      <footer className="footer">
        <p>Todos los derechos reservados. ©</p>
      </footer>
    );
  }
}
