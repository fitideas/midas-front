import * as React from 'react';
import TextField from '@mui/material/TextField';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import DateAdapter from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DesktopDatePicker from '@mui/lab/DesktopDatePicker';
import Button from '@mui/material/Button';
import { DataAPIService } from '../../services/data-api.service';
import './index.css';
import { FilterData } from '../../model/IfilterData';
import {useSnackbar} from "notistack";

interface IPropertySearch {
  seachData: (filter: FilterData) => void;
}

const Search: React.FC<IPropertySearch> = ({ seachData }) => {
  const [selectedTipoDoc, setTipoDoc] = React.useState('');
  const [numeroDocumento, setnumeroDocumento] = React.useState('');
  const [numeroSuministro, setnumeroSuministro] = React.useState('');
  const [numeroContrato, setnumeroContrato] = React.useState('');
  const [numeroServicio, setnumeroServicio] = React.useState('');

  const handleChangeSelectRipoDoc = (event: SelectChangeEvent) => {
    setTipoDoc(event.target.value);
  };

  const [slectedSubproducto, setSubproducto] = React.useState('');

  const handleChangeSelectSubproducto = (event: SelectChangeEvent) => {
    setSubproducto(event.target.value);
  };

  const [dateDesde, setDateDesde] = React.useState<Date | null>(null);

  const handleChangeDateDesde = (newValue: Date | null) => {
    setDateDesde(newValue);
  };

  const handleChangeNumeroDocumento = (event: React.ChangeEvent<HTMLInputElement>) => {
    setnumeroDocumento(event.target.value);
  };

  const handleChangeNumeroSuministro = (event: React.ChangeEvent<HTMLInputElement>) => {
    setnumeroSuministro(event.target.value);
  };

  const handleChangeNumeroContrato = (event: React.ChangeEvent<HTMLInputElement>) => {
    setnumeroContrato(event.target.value);
  };

  const handleChangeNumeroServicio = (event: React.ChangeEvent<HTMLInputElement>) => {
    setnumeroServicio(event.target.value);
  };

  const [dateHasta, setDateHasta] = React.useState<Date | null>(null);
  const {enqueueSnackbar} = useSnackbar();
  const handleChangeDateHasta = (newValue: Date | null) => {
    setDateHasta(newValue);
  };

  interface Subproducto {
    tipoProductoDto?: any;
    codigo?: number;
    nombre?: string;
    productoPadre?: any;
  }
  const [subproductos, setSubproductos] = React.useState<Subproducto[]>([]);
  const [tiposDoc, setTiposDoc] = React.useState<Subproducto[]>([]);

  React.useEffect(() => {
    let api = DataAPIService;

    api.getSubproductos().then((response: any) => {
      setSubproductos(response.data.data);
    });

    api.getTiposDoc().then((response: any) => {
      setTiposDoc(response.data.data);
    });
  }, []);

  const onclick_button = () => {
    if(dateDesde!=null && dateDesde>new Date()||dateHasta!=null && dateHasta>new Date()){
      enqueueSnackbar('La fecha seleccionada es invalida', {variant: 'error'});
      return
    }
    seachData(
      new FilterData(
        selectedTipoDoc !== '' ? Number(selectedTipoDoc) : null,
        numeroDocumento !== '' ? Number(numeroDocumento) : null,
        numeroSuministro !== '' ? Number(numeroSuministro) : null,
        numeroContrato !== '' ? Number(numeroContrato) : null,
        slectedSubproducto !== '' ? Number(slectedSubproducto) : null,
        dateDesde,
        dateHasta,
        numeroServicio !== '' ? Number(numeroServicio) : null,
      ),
    );
  };

  return (
    <div className="search">
      <div className="search-row">
        <FormControl>
          <InputLabel id="tipo-doc-select-label">Tipo de Documento</InputLabel>
          <Select
            labelId="tipo-doc-select-label"
            id="tipo-doc-select"
            value={selectedTipoDoc}
            label="Tipo de Documento"
            onChange={handleChangeSelectRipoDoc}
          >
            <MenuItem value="">
              <em>No aplica</em>
            </MenuItem>
            {tiposDoc.map((TipoDoc) => {
              return (
                <MenuItem key={TipoDoc.codigo} value={TipoDoc.codigo}>
                  {TipoDoc.nombre}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>

        <FormControl>
          <InputLabel id="subprodcuto-select-label">Subproducto</InputLabel>
          <Select
            labelId="subprodcuto-select-label"
            id="subprodcuto-select"
            value={slectedSubproducto}
            label="Subproducto"
            onChange={handleChangeSelectSubproducto}
          >
            <MenuItem value="">
              <em>No aplica</em>
            </MenuItem>
            {subproductos.map((Subproducto) => {
              return (
                <MenuItem key={Subproducto.codigo} value={Subproducto.codigo}>
                  {Subproducto.nombre}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>

        <TextField
          onChange={handleChangeNumeroSuministro}
          value={numeroSuministro}
          id="outlined-basic"
          label="Número de Suministro"
          variant="outlined"
          InputProps={{ inputProps: { type: 'number', min: 0 } }}
        />
        <TextField
          onChange={handleChangeNumeroContrato}
          value={numeroContrato}
          InputProps={{ inputProps: { type: 'number', min: 0 } }}
          id="outlined-basic"
          label="Número de Contrato"
          variant="outlined"
        />

        <TextField
          onChange={handleChangeNumeroServicio}
          value={numeroServicio}
          InputProps={{ inputProps: { type: 'number', min: 0 } }}
          id="outlined-basic"
          label="Número de Servicio"
          variant="outlined"
        />
      </div>

      <div className="search-row-two">
        <TextField
          onChange={handleChangeNumeroDocumento}
          value={numeroDocumento}
          id="outlined-basic"
          type="number"
          label="Número de documento"
          variant="outlined"
        />
        <LocalizationProvider dateAdapter={DateAdapter}>
          <DesktopDatePicker
            label="Desde"
            value={dateDesde}
            onChange={handleChangeDateDesde}
            allowSameDateSelection={true}
            inputFormat="dd-MM-yyyy"
            maxDate={new Date()}
            mask="__-__-____"
            renderInput={(params) => <TextField {...params} />}
          />
          <DesktopDatePicker
            label="Hasta"
            value={dateHasta}
            mask="__-__-____"
            inputFormat="dd-MM-yyyy"
            allowSameDateSelection={true}
            maxDate={new Date()}
            onChange={handleChangeDateHasta}
            renderInput={(params) => <TextField {...params} />}
          />
        </LocalizationProvider>
        <Button variant="contained" onClick={onclick_button}>
          Buscar
        </Button>
      </div>
    </div>
  );
};
export default Search;
