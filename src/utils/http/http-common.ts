import axios from "axios";
import { environment } from "../../environments/environment";

export default axios.create({
  baseURL: environment.urlApi,
  headers: {
    "Content-type": "application/json",
    "x-api-key": environment.api_key
  }
});