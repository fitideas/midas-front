import {FC} from 'react';
import {Route, Routes} from 'react-router-dom';
import Consultas from '../pages/consultas';
import HomePage from '../pages/home';
import HomePage2 from '../pages/home2';
import CalendarioFacturacion from '../pages/calendario_facturacion';
import GenerarCartaMorosos from '../pages/generar_carta_morosos';
import GenerarSegundoPreaviso from '../pages/generar_segundo_preaviso';

import EliminarCartaMorosos from "../pages/eliminar_carta_morosos";
import CargueReclamaciones from "../pages/cargue_reclamaciones";
import AfectosGastosCobranza from "../pages/afectos_gastos_cobranza";
import CasasDeCobranza from "../pages/casas_de_cobranza";
import CargueGestionCobro from "../pages/cargar_gestion_cobro";

const AppRoutes: FC = () => {
    return (
        <Routes>
            <Route path="/" element={<HomePage/>}/>
            <Route path="/home" element={<HomePage2/>}/>
            <Route path="/consultas" element={<Consultas/>}/>
            <Route path="/calendarioFacturacion" element={<CalendarioFacturacion/>}/>
            <Route path="/generarCartasMorosos" element={<GenerarCartaMorosos/>}/>
            <Route path="/casasDeCobranza" element={<CasasDeCobranza/>}/>
            <Route path="/generarSegundoPreaviso" element={<GenerarSegundoPreaviso/>}/>
            <Route path="/eliminarCartasMorosos" element={<EliminarCartaMorosos/>}/>
            <Route path="/cargueReclamaciones" element={<CargueReclamaciones/>}/>
            <Route path="/cargueGestionCobro" element={<CargueGestionCobro/>}/>
            <Route path="/AfectosGastosCobranza" element={<AfectosGastosCobranza/>}/>
        </Routes>
    );
};

export default AppRoutes;
