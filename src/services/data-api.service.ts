import {FilterData} from '../model/IfilterData';
import http from '../utils/http/http-common';
import {AxiosResponse} from "axios";

export class DataAPIService {
  public static getFile(filter: FilterData) {
    return http.get('/schedule/file', { responseType: 'blob', params:  filter  });
  }

  public static getCatera(filter: FilterData) {
    return http.get('/cartera/carteraLigth', { params: filter });
  }

  public static getRefinanciaciones(filter: FilterData) {
    return http.get('/refinanciaciones', { params: filter });
  }

  public static getRecaudo(filter: FilterData) {
    return http.get('/recaudo/historico', { params: filter });
  }

  public static getFacturacion(filter: FilterData) {
    filter.verMas = false;
    return http.get('/facturacion', { params: filter });
  }

  public static getCateraHistorica(filter: FilterData) {
    return http.get('/cartera/historico', { params: filter });}

  public static async getSubproductos() {
    return http.get("/catalogue/subProduct");
  }

  public static async getTiposDoc() {
    return http.get("/catalogue/documentType");
  }

  public static getDummy()
  {
    return  http.get('/catalogue/file/get_dummy');
  }

  public static getCondonacionHistorico(filter: FilterData){
    return http.get('/condonacion/condonaciones', {params:filter});
  }

  /**
   * Get The Cambio Estado registers based on the Filter
   * @param filter Promise from the backend
   */
  public static getCambioEstado(filter: FilterData): Promise<AxiosResponse>{
    return http.get('/cambioEstado', {params: filter})
  }

  public static getTraslados(filter: FilterData): Promise<AxiosResponse>{
    return http.get('/traslado', {params: filter})
  }

  // Calendario Facturacion Services

  /**
   * Get the last Calendario Facturacion loaded
   */
  public static getLastCalendarioFacturacion(): Promise<AxiosResponse> {
    return http.get('/calendarioFacturacion/last');
  }

  /**
   * Check if a version of Calendario Facturacion is repeated
   */
  public static isVersionNoRepeated(version: string): Promise<AxiosResponse>{
    return http.get('/calendarioFacturacion/validate', {
      params: {
        version: version
      }
    })
  }

  /**
   * Upload a new version of Calendario Facturacion
   * @param file excel or csv file
   * @param version version of upload
   * @param onUploadProgress callback
   */
  public static uploadCalendarioFacturacion(file: File, version: string, onUploadProgress: (progressEvent: ProgressEvent) => void): Promise<AxiosResponse> {
    let formData: FormData = new FormData();
    formData.append("file", file);
    return http.post('/calendarioFacturacion/upload', formData, {
      headers: {
        "Content-Type": "multipart/form-data"
      },
      params: {
        version: version
      },
      onUploadProgress
    });
  }
  /**
   * Delete the letters to overdued
   * @param file excel or csv file
   */
  public static deleteCartasMorosos(file: File): Promise<AxiosResponse> {
    let formData: FormData = new FormData();
    formData.append("file", file);
    return http.delete('/cartaMorosa/delete',{
      data: formData,
      headers: {
        "Content-Type": "multipart/form-data"
      }
    } );
  }
  /**
   * Upload reclamations
   * @param file excel file
   */
  public static uploadReclamaciones(file: File): Promise<AxiosResponse> {
    let formData: FormData = new FormData();
    formData.append("file", file);
    return http.post('/reclamacion/upload',formData,{
      headers: {
        "Content-Type": "multipart/form-data"
      }
    } );
  }
  /**
   * Upload reclamations
   * @param file excel file
   */
  public static uploadGestionCobro(file: File): Promise<AxiosResponse> {
    let formData: FormData = new FormData();
    formData.append("file", file);
    return http.post('/gestionCobro/upload',formData,{
      headers: {
        "Content-Type": "multipart/form-data"
      }
    } );
  }

  /**
   * Download Report from server
   * @param name of the report to download
   */
  public static getReport(name: FilterData) {
    return http.get('/reports/download', { responseType: 'blob', params:  {
      name:name
      }  });
  }
  /**
   * Get the number of Afectos Gasto Cobranza
   */
  public static getNumberAfectos(): Promise<AxiosResponse>{
    return http.get('/generar-gastos-cobranza')
  }

  // Generar Carta Morosos Services

  public static generateCartaMorosos(statusList: string[], facturationCycle: number, ext: number): Promise<AxiosResponse>{
    return http.get('/cartaMorosa/generar', {
      responseType: 'blob',
      params: {
        estados: statusList.join(","),
        ciclo: facturationCycle,
        ext: ext
      }
    });
  }

  public static generarSegundoPreaviso(ext: number): Promise<AxiosResponse>{
    return http.get('/segundoPreaviso', { 
      responseType: 'blob',
      params: {
        ext: ext
      }
    });
  }

  // Casas de cobranza services

  /**
   * Get Last date of cartera load
   */
  public static getLastFechaCartera(): Promise<AxiosResponse>{
    return http.get("/getLastCarteraDate");
  }

  /**
   * Generate casas de cobranza information
   */
  public static generarCasasDeCobranza(): Promise<AxiosResponse>{
    return http.post("/casas-de-cobranza/");
  }

}
