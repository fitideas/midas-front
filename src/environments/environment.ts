/**
 * Environment Variables configuration
 */

/**
 * Environment variables types definitions
 */
interface Environment {
    urlApi: string,
    pageSize: number,
    api_key: string
}

/**
 * Returns an object with environment variables mapping
 */
export const environment: Environment = {
    urlApi: process.env.REACT_APP_MIDAS_API_URL != undefined ? process.env.REACT_APP_MIDAS_API_URL : 'http://3.232.112.222:1994/',
    pageSize: process.env.REACT_APP_MIDAS_PAGE_SIZE != undefined ? parseInt(process.env.REACT_APP_MIDAS_PAGE_SIZE) : 30,
    api_key: process.env.REACT_APP_MIDAS_API_KEY != undefined ? process.env.REACT_APP_MIDAS_API_KEY : 'AAAAB3NzaC1yc2EAAAADAQABAAABgQDdsjzPlF8NCoFcyoeMBg5oujmwwp6tsajCqCZFHieEnFDEFVCYpM5zo8cOLYv4gVi+4Ig3J72g39bspzAmqq6sTwSIH7f9FgeaK/0N6chtta844pntvIOx6ck2VJZIes26YYnjh7Giy2JS5Yc04M3jXropmz5f/AXo99W7GskddTP3qujxs93LOb2tj0hB3GmK2qPxcVD4UA8BRhXggfJp5Nkhc0Ts2eEtlSMRFhm/a9NvSk0hKL6QSDUkbV87cZHfWFQ3vfhJVIg7ASTDlyg+zpdBuDkqeYBMI99C8cspgZwc1wIUlZGzl0CkQnyTVx5cUnDpTU35Kk2jqwb8VwmijIxSBiWBX0M8DQgEK/DjypRU3uaZgaQUM5FovuicfQ0BIWoYWCQcS0+Z5JTUmVIcRjmgPak/pjQaIYdr4nOjoXRmg4xuowdxJNfDR7j9lbQg2rnsr4n4F/3NpVxw9sXRluvm+jlo0Ewg0MbpDeJJFZ2hyIlK4+en3zNOjTtTREk'
};
