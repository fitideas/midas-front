/**
 * Eliminar carta a morosos Page
 */
import React from "react";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import {useSnackbar} from "notistack";
import CircularProgress from "@mui/material/CircularProgress";
import {Dialog, DialogActions, DialogTitle} from "@mui/material";
import {DataAPIService} from "../../services/data-api.service";
import {DeleteResult} from "../../models/deleteResult";
import {downloadFile} from "../../utils/grid/grid-view-components";

const EliminarCartaMorosos = (): JSX.Element => {
    /**
     * File React state
     */
    const [file, setFile] = React.useState<File | null>(null);
    /**
     * Loading React State
     */
    const [loading, setLoading] = React.useState<boolean>(false);
    /**
     * Open React State
     */
    const [open, setOpen] = React.useState(false);
    /**
     * deleteResult React State
     */
    const [deleteResult, setDeleteResult] = React.useState<DeleteResult | null>(null);
    /**
     * Notification Stack Context
     */
    const snackbarContext = useSnackbar();
    /**
     * On File Change handler
     * @param event Change Event
     */
    const onChangeFile = (event: React.ChangeEvent<HTMLInputElement>) => {
        let fileList: FileList | null = event.target.files;
        if(fileList != null){
            setFile(fileList[0]);
            snackbarContext.enqueueSnackbar('Archivo cargado exitosamente.', {variant: 'success'});
        }else{
            setFile(null);
        }
        setDeleteResult(null);
    }
    /**
     * onDelete button handler
     */
    const onDelete = () => {
        setOpen(true);
    };
    /**
     * onConfirm delete button handler
     */
    const onConfirm = () => {
        if(file != null){
            setOpen(false);
            setLoading(true);
            DataAPIService.deleteCartasMorosos(file).then(response => {
                if (response.data.data !=null){
                    setDeleteResult(response.data.data);
                    if(response.data.data.failed>0){
                        DataAPIService.getReport(response.data.data.file).then((file)=>{
                            let names = response.data.data.file.split("/")
                            downloadFile(file.data,names[names.length-1])
                        }

                        )
                    }
                }
                setLoading(false);
            }, reason => {
                console.log(reason);
                setLoading(false);
                snackbarContext.enqueueSnackbar('Ha ocurrido un error procesando la información: '+reason.response.data.message, {variant: 'error'});
            })
        }
    };
    /**
     * onCloseDialogHandler
     */
    const handleClose = () => {
        setOpen(false);
    };
    return (
        <Grid container justifyContent="center" alignContent="center" padding="4rem" textAlign="center" spacing={2}>
            <Grid item xs={12}>
                <Typography variant="h4" gutterBottom component="div">
                    Eliminación de cartas a morosos
                </Typography>
            </Grid>
            <Grid item xs={12}>
                <Button variant="contained" component="label">
                    Subir archivo
                    <input accept=".txt" type="file" hidden={true} onChange={onChangeFile}/>
                </Button>
            </Grid>
            <Grid item xs={12}>
                <Typography variant="h6" gutterBottom component="div">
                    {file == null ? "" : "Archivo cargado: \"" + file.name + "\""}
                </Typography>
            </Grid>
            <Grid item  xs={12} textAlign="center">
                {file != null && (
                    <Button variant="contained" component="label" onClick={onDelete} disabled={loading || deleteResult != null}>
                        Eliminar Registros
                    </Button>
                )}
            </Grid>
            <Grid item xs={12}>
                {loading && <CircularProgress  variant={'indeterminate'}/>}
            </Grid>
            <Grid item xs={12}>
                {loading && "Procesando archivo"}
            </Grid>
            {deleteResult != null&&(
                <Grid item container xs={3}  alignContent={"center"} justifyContent={"center"} >
                    <Grid item xs={12}>
                        <Typography variant="h5" gutterBottom component="div">
                            Resultado de la operación
                        </Typography>
                    </Grid>
                    <Grid item xs={6}>
                        Registros Procesados
                    </Grid>
                    <Grid item xs={3}>
                        {deleteResult.deleted+deleteResult.failed}
                    </Grid>
                    <Grid item xs={6}>
                        Registros Eliminados
                    </Grid>
                    <Grid item xs={3}>
                        {deleteResult.deleted}
                    </Grid>
                    <Grid item xs={6}>
                        Registros No Eliminados
                    </Grid>
                    <Grid item xs={3}>
                        {deleteResult.failed}
                    </Grid>
                </Grid>
            )}
            <Dialog
                open={open}
                onClose={handleClose}
            >
                <DialogTitle >
                    {"¿Esta seguro que desea eliminar estos registros?"}
                </DialogTitle>
                <DialogActions>
                    <Button onClick={handleClose} autoFocus>
                        Cancelar
                    </Button>
                    <Button onClick={onConfirm} color={"error"}>Eliminar</Button>
                </DialogActions>
            </Dialog>
        </Grid>
    );
}
export default EliminarCartaMorosos;