import {FilterData} from "../../model/IfilterData";
import {ConsultaCaller, MidasCols} from "./call";
import {DataAPIService} from "../../services/data-api.service";

export function callCartera(filter: FilterData, consultaCaller: ConsultaCaller) {
    const columns: MidasCols[] = [
        {
            field: 'nombre_cliente',
            headerName: 'Nombre cliente',
            width: 125,
        },
        {
            field: 'intereses_corrientes',
            headerName: 'Intereses corrientes',
            type: 'number',
            width: 150,
        },
        {
            field: 'intereses_mora',
            headerName: 'Intereses de mora',
            type: 'number',
            width: 140,
        },
        {
            field: 'cuota_manejo',
            headerName: 'Cuota de manejo',
            type: 'number',
            width: 140,
        },
        {
            field: 'gastos_cobranzas',
            headerName: 'Gastos de cobranzas',
            type: 'number',
            width: 160,

        },
        {
            field: 'seguros',
            headerName: 'Seguro',
            type: 'number',
            width: 100,

        },
        {
            field: 'numero_compra',
            headerName: 'Numero de compra',
            type: 'number',
            width: 160,
        },
        {
            field: 'nuevos_cobros',
            headerName: 'Nuevos cobros',
            type: 'number',
            width: 140,
        },
        {
            field: 'dias_mora',
            headerName: 'Días de mora',
            type: 'number',
            width: 140,
        },
        {
            field: 'estado_compra',
            headerName: 'Estado de la compra',
            width: 160,
        },
    ];
    consultaCaller.Call(columns,filter,DataAPIService.getCatera)
}