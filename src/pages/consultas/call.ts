import { GridColDef, GridValueFormatterParams } from '@mui/x-data-grid';
import React from 'react';
import { FilterData } from '../../model/IfilterData';
import { AxiosResponse } from 'axios';

export interface MidasCols {
  field: string;
  headerName: string;
  width?: number;
  type?: string | undefined;
  flex?: number | undefined;
}
export class ConsultaCaller {
  setIsLoading: React.Dispatch<boolean>;
  setRowCountState: React.Dispatch<number>;
  setColumnsData: React.Dispatch<GridColDef[]>;
  setRows: React.Dispatch<never[]>;
  manageError: Function;

  constructor(
    setIsLoading: React.Dispatch<boolean>,
    setRowCountState: React.Dispatch<number>,
    setColumnsData: React.Dispatch<GridColDef[]>,
    setRows: React.Dispatch<never[]>,
    manageError: Function,
  ) {
    this.setIsLoading = setIsLoading;
    this.setRowCountState = setRowCountState;
    this.setColumnsData = setColumnsData;
    this.setRows = setRows;
    this.manageError = manageError;
  }

  formatColumns(params: GridValueFormatterParams, value: MidasCols) {
    if (value.type === 'number')
      return params.value
        ?.toString()
        .replace(/\./g, ',')
        .replace(/\,/g, '.')
        .replace(/\B(?=(\d{3})+(?!\d))/g, '.');
    if(value.type === 'money'){
        return Math.trunc(Number(params.value)).toString();
    }
    else return params.value;
  }

  setType(value: MidasCols) {
    if (value.type === 'money') 
        return 'number';
    else return value.type;
  }

  Call(columns: MidasCols[], filter: FilterData, apiCall: Function) {
    let gridColumns: GridColDef[] = columns.map<GridColDef>((value) => {
      return {
        field: value.field,
        headerName: value.headerName,
        width: value.width,
        type: this.setType(value),
        flex: value.flex,
        description: value.headerName,
        editable: false,
        hideable: false,
        valueFormatter: (params: GridValueFormatterParams) => {
          return this.formatColumns(params, value);
        },
        filterable: false,
        disableColumnMenu: true,
        align:"center",
        headerAlign:"center",
      };
    });
    this.setIsLoading(true);
    this.setRows([]);
    this.setColumnsData(gridColumns);
    apiCall(filter)
      .then((data: AxiosResponse<any>) => {
        this.setRows(data.data.data.content);
        this.setRowCountState(data.data.data.total_elements);
        this.setIsLoading(false);
      })
      .catch((data: any) => {
        this.manageError(data);
        this.setIsLoading(false);
      });
  }
}
