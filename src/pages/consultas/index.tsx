import { AppBar, Tab, Tabs } from '@mui/material';
import { DataGrid, GridColDef } from '@mui/x-data-grid';
import React, { useState } from 'react';
import { ConsultaEnum } from '../../model/enum/consultas_enum';
import { CustomToolbar, CustomNoRowsOverlay } from '../../utils/grid/grid-view-components';
import Search from '../../components/searchFields';
import { useSnackbar } from 'notistack';
import { FilterData } from '../../model/IfilterData';
import { HttpStatusCode } from '../../model/enum/HttpStatusCodes';
import {ConsultaCaller} from "./call";
import {callCartera} from "./callCartera";
import {callCarteraHistorica} from "./callCarteraHistorica";
import {callRefinanciaciones} from "./callRefinanciaciones";
import {callCondonacionHistorico} from "./callCondonacionHistorico";
import {callRecaudo} from "./callRecaudo";
import {callCambioEstado} from "./callCambioEstado";
import { environment } from '../../environments/environment';
import {callFacturacion} from "./callFacturacion";
import { callTraslados } from './callTraslado';

const Consultas = () => {
  const [rows, setRows] = useState([]);
  const [columnsData, setColumnsData] = useState<GridColDef[]>([]);
  const [value, setValue] = useState(0);
  const [tabSelected, setTabSelected] = useState(0);
  const [rowCountState, setRowCountState] = useState(0);
  const [seachQuery, setSearchQuery] = useState(false);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [consultaEnum, setConsultaEnum] = useState<ConsultaEnum>(ConsultaEnum.Cliente);
  const [consultaString, setConsultaString] = useState('Consulta');

  const [filterDataObj, setFilterData] = useState<FilterData>(new FilterData());

  const {enqueueSnackbar} = useSnackbar();

  const manageError = (data: any) => {
    if (data.response.status === HttpStatusCode.BadRequest.valueOf()) {
      enqueueSnackbar(data.response.data.message, {variant: 'warning'});
      return;
    }
    console.log(data);
    enqueueSnackbar('Ocurrio un error durante la consulta de los datos', {variant: 'error'});
  }
  const consultaCaller = new ConsultaCaller(setIsLoading, setRowCountState, setColumnsData, setRows, manageError);

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
    setTabSelected(newValue);
    callData(newValue, filterDataObj, seachQuery);
  };

  const callData = (newValue: number, filter: FilterData, seachQueryFlag: boolean) => {
    if (newValue === 0 && seachQueryFlag) {
      setConsultaEnum(ConsultaEnum.Cliente);
      setConsultaString('Cliente_cartera');
      callCartera(filter,consultaCaller);
    }
    if (newValue === 1 && seachQueryFlag) {      
      setConsultaEnum(ConsultaEnum.ClienteHistorico);
      setConsultaString('Cliente_CarteraHistórica');
      callCarteraHistorica(filter,consultaCaller);
    }
    if (newValue === 2 && seachQueryFlag) {
      callRefinanciaciones(filter,consultaCaller);
      setConsultaEnum(ConsultaEnum.ClienteRefinanciaciones);
      setConsultaString('Cliente_refinanciaciones');
    }
    if (newValue === 3 && seachQueryFlag) {
      callCondonacionHistorico(filter,consultaCaller);
      setConsultaEnum(ConsultaEnum.ClienteCondonaciones);
      setConsultaString('Cliente_condonaciones');
    }
    if (newValue === 4 && seachQueryFlag) {
      callFacturacion(filter,consultaCaller);
      setConsultaEnum(ConsultaEnum.ClienteFacturacion);
      setConsultaString('Cliente_facturacion');
    }
    if (newValue === 5 && seachQueryFlag) {
      callRecaudo(filter,consultaCaller);
      setConsultaEnum(ConsultaEnum.ClienteRecaudo);
      setConsultaString('Cliente_recaudo');
    }

    if(newValue === 6 && seachQueryFlag){
      callCambioEstado(filter, consultaCaller);
      setConsultaEnum(ConsultaEnum.ClienteCambioEstado)
      setConsultaString('Cliente_cambio_estado');
    }

    if(newValue === 7 && seachQueryFlag){
      callTraslados(filter, consultaCaller);
      setConsultaEnum(ConsultaEnum.ClienteTraslados)
      setConsultaString('Cliente_traslados');
    }
  };

  const callback = (filter: FilterData) => {
    setFilterData(filter);
    setSearchQuery(true);
    callData(tabSelected, filter, true);
  };

  const changePage = (page: number) =>
  {
    let filter_page = filterDataObj;
    filter_page.offset=page;
    callData(tabSelected, filter_page, true);
  }

  return (
    <div>
      <Search seachData={callback}/>
      <div style={{ margin: '1% auto 10% auto', height: 450, width: '70%' }}>
        <AppBar position="static">
          <Tabs
            indicatorColor="secondary"
            textColor="inherit"
            variant="scrollable"
            value={value}
            color="primary"
            scrollButtons="auto"
            onChange={handleChange}
            // centered
          >
            <Tab label="Cartera actual" />
            <Tab label="Cartera histórica" />
            <Tab label="Refinanciaciones" />
            <Tab label="Negociaciones" />
            <Tab label="Facturación" />
            <Tab label="Recaudo" />
            <Tab label="Cambio estado" />
            <Tab label="Traslados" />
          </Tabs>
        </AppBar>
        <DataGrid
          columns={columnsData}
          rows={rows}
          rowsPerPageOptions={[5]}
          rowCount={rowCountState}

          loading={isLoading}
          pageSize={environment.pageSize}
          getRowId={(row) => row.id}
          disableSelectionOnClick
          pagination
          paginationMode="server"
          onPageChange={changePage}
          components={{
            Toolbar: () => CustomToolbar(consultaEnum, consultaString, filterDataObj),
            NoRowsOverlay: CustomNoRowsOverlay,
            NoResultsOverlay: CustomNoRowsOverlay
          }}
        />
      </div>
    </div>
  );
};

export default Consultas;