import {FilterData} from "../../model/IfilterData";
import {ConsultaCaller, MidasCols} from "./call";
import {DataAPIService} from "../../services/data-api.service";

export function callRecaudo(filter: FilterData, consultaCaller: ConsultaCaller) {
    const columns: MidasCols[] =  [
        {
            field: 'fecha_de_pago',
            headerName: 'Fecha de pago',
            type: 'string',
            width:120,
        },
        {
            field: 'fecha_proceso_pago',
            headerName: 'Fecha proceso pago',
            type: 'string',
            width:160,
        },
        {
            field: 'capital',
            headerName: 'Capital',
            type: 'number',
            width:160,
        },
        {
            field: 'intereses_corrientes',
            headerName: 'Intereses corriente',
            type: 'number',
            width:160,
        },
        {
            field: 'intereses_de_mora',
            headerName: 'Intereses de mora',
            type: 'number',
            width:160,
        },
        {
            field: 'cuota_de_manejo',
            headerName: 'Cuota de manejo',
            type: 'number',
            width:160,
        },
        {
            field: 'gastos_de_cobranza',
            headerName: 'Gastos de cobranza',
            type: 'number',
            width:160,
        },
        {
            field: 'seguro_obligatorio',
            headerName: 'Seguro obligatorio',
            type: 'number',
            width:160,
        },
        {
            field: 'seguro_voluntario',
            headerName: 'Seguro voluntario',
            type: 'number',
            width:160,
        }
    ];
    consultaCaller.Call(columns,filter,DataAPIService.getRecaudo)
}