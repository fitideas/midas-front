import {FilterData} from "../../model/IfilterData";
import {ConsultaCaller, MidasCols} from "./call";
import {DataAPIService} from "../../services/data-api.service";

export function callCondonacionHistorico(filter: FilterData, consultaCaller: ConsultaCaller) {
    const columns: MidasCols[] =  [
        {
            field: 'fecha_negociacion',
            headerName: 'Fecha negociacion',
            width:160,
        },
        {
            field: 'fecha_aplicacion',
            headerName: 'Fecha Aplicación',
            type: 'date',
            width:130,
        },
        {
            field: 'fecha_rechazo',
            headerName: 'Fecha Rechazo',
            type: 'date',
            width:120,
        },
        {
            field: 'fecha_limite_pago',
            headerName: 'Fecha límite de Pago',
            type: 'date',
            width:150,
        },
        {
            field: 'estado_condonacion',
            headerName: 'Estado condonación',
            width:160,
        },
        {
            field: 'usuario',
            headerName: 'Usuario',
            width:90,
        },
        {
            field: 'area_modificacion',
            headerName: 'Area Modificación',
            width:135,
        },
        {
            field: 'tipo_condonacion',
            headerName: 'Tipo condonación',
            width:160,
        },
        {
            field: 'capital_condonado',
            headerName: 'Capital condonado',
            type: 'number',
            width:140,
        },
        {
            field: 'interes_corriente',
            headerName: 'Interés Corriente',
            type: 'number',
            width:150,
        },
        {
            field: 'interes_moratorio',
            headerName: 'Interes mora',
            type: 'number',
            width:110,
        },

        {
            field: 'subproducto',
            headerName: 'Subproducto',
            width:160,
        },
        {
            field: 'numero_identificacion',
            headerName: 'Número identificación cliente',
            type: 'string',
            width:210,
        },
        {
            field: 'numero_servicio',
            headerName: 'Número Servicio',
            type: 'string',
            width:160,
        },
        {
            field: 'id_negociacion',
            headerName: 'Id Negociación',
            type: 'string',
            width:160,
        },
        {
            field: 'valor_pago',
            headerName: 'Valor Pago',
            type: 'number',
            width:160,
        },
        {
            field: 'cuotas',
            headerName: 'Cuotas',
            type: 'number',
            width:160,
        },
        {
            field: 'seguros',
            headerName: 'Seguros',
            type: 'number',
            width:160,
        },
        {
            field: 'gastos_cobranza',
            headerName: 'Gastos cobranza',
            type: 'number',
            width:160,
        },
    ];
    consultaCaller.Call(columns,filter,DataAPIService.getCondonacionHistorico)
}