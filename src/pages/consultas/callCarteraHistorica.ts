import {FilterData} from "../../model/IfilterData";
import {ConsultaCaller, MidasCols} from "./call";
import {DataAPIService} from "../../services/data-api.service";

export function callCarteraHistorica(filter: FilterData, consultaCaller: ConsultaCaller) {
    const columns: MidasCols[] =  [
        {
            field: 'nombre_cliente',
            headerName: 'Nombre cliente',
            width: 125,
        },
        {
            field: 'capital_impago',
            headerName: 'Capital impago',
            type: 'money',
            width: 140,
        },
        {
            field: 'saldo_capital_por_facturar',
            headerName: 'Saldo capital por facturar',
            type: 'money',
            width: 200,
        },
        {
            field: 'interes_corriente_orden_causado_no_facturado',
            headerName: 'Interés corriente orden causado no facturado',
            type: 'money',
            width: 330,
        },
        {
            field: 'interes_orden_corriente_impago',
            headerName: 'Interés orden corriente impago',
            type: 'money',
            width: 230,
        },
        {
            field: 'total_interes_corriente_causado',
            headerName: 'Total interés corriente causado',
            type: 'money',
            width: 238,
        },
        {
            field: 'interes_mora',
            headerName: 'Interés de mora',
            type: 'money',
            width: 125,
        },
        {
            field: 'interes_mora_causado_no_facturado',
            headerName: 'Interés mora causado no facturado',
            type: 'money',
            width: 253,
        },
        {
            field: 'interes_mora_orden_causado_no_facturado',
            headerName: 'Interés mora orden causado no facturado',
            type: 'money',
            width: 299,
        },
        {
            field: 'interes_orden_mora_impago',
            headerName: 'Interés orden mora impago',
            type: 'money',
            width: 202,
        },
        {
            field: 'total_interes_mora_causado',
            headerName: 'Total interés mora causado',
            type: 'money',
            width: 199,
        },
        {
            field: 'total_interes_mora_facturado_impago',
            headerName: 'Total interés mora facturado impago',
            type: 'money',
            width: 269,
        },
        {
            field: 'dias_atraso_maximo',
            headerName: 'Días de atraso máximo',
            type: 'number',
            width: 171,
        },
        {
            field: 'mora_primer_vencimiento_maximo',
            headerName: 'Mora primer vencimiento máximo',
            type: 'money',
            width: 240,
        },
        {
            field: 'estado_contrato',
            headerName: 'Estado contrato',
            width: 125,
        },
        {
            field: 'asignacion_casa_cobranzas',
            headerName: 'Asignación de casa de cobranzas',
            width: 238,
        },
    ];
    consultaCaller.Call(columns,filter,DataAPIService.getCateraHistorica)
}