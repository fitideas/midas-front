import {FilterData} from "../../model/IfilterData";
import {ConsultaCaller, MidasCols} from "./call";
import {DataAPIService} from "../../services/data-api.service";

export function callFacturacion(filter: FilterData, consultaCaller: ConsultaCaller) {
    const columns: MidasCols[] =  [
        {
            field: 'numero_contrato',
            headerName: 'Contrato',
            type: 'number',
            width:110,
        },
        {
            field: 'subproducto',
            headerName: 'Subproducto',
            type: 'string',
            width:120,
        },
        {
            field: 'fecha_documento',
            headerName: 'Fecha del documento',
            type: 'string',
            width:160,
        },
        {
            field: 'fecha_contable',
            headerName: 'Fecha contable',
            type: 'string',
            width:130,
        },
        {
            field: 'capital',
            headerName: 'Capital',
            type: 'number',
            width:105,
        },
        {
            field: 'interes_corriente',
            headerName: 'Interes corriente',
            type: 'number',
            width:130,
        },
        {
            field: 'interes_mora',
            headerName: 'Interes mora',
            type: 'number',
            width:105,
        },
        {
            field: 'cuota_manejo',
            headerName: 'Cuota de manejo',
            type: 'number',
            width:135,
        },
        {
            field: 'gastos_cobranza',
            headerName: 'Gastos de cobranza',
            type: 'number',
            // flex: 1,
            width:150,
        },
        {
            field: 'seguro_obligatorio',
            headerName: 'Seguro obligatorio',
            type: 'number',
            // flex: 1,
            width:140,
        },
        {
            field: 'seguro_voluntario',
            headerName: 'Seguro voluntario',
            type: 'number',
            // flex: 1,
            width:135,
        },
        {
            field: 'nuevos_cobros',
            headerName: 'Nuevos cobros',
            type: 'number',
            // flex: 1,
            width:105,
        }
    ];
    consultaCaller.Call(columns,filter,DataAPIService.getFacturacion)
}