import {FilterData} from "../../model/IfilterData";
import {ConsultaCaller, MidasCols} from "./call";
import {DataAPIService} from "../../services/data-api.service";

export function callCambioEstado(filter: FilterData, consultaCaller: ConsultaCaller) {
    const columns: MidasCols[] = [
        {
            field: 'cedula_del_titular',
            headerName: 'Cédula del titular',
            width: 160
        },
        {
            field: 'numero_de_servicio_o_compra',
            headerName: 'Número del Servicio o Compra',
            width: 160
        },
        {
            field: 'subproducto',
            headerName: 'Subproducto',
            width: 160
        },
        {
            field: 'estado_anterior',
            headerName: 'Estado anterior',
            width: 160
        },
        {
            field: 'estado_actual',
            headerName: 'Estado actual',
            width: 160
        },
        {
            field: 'fecha_de_cambio_de_estado',
            headerName: 'Fecha Cambio de Estado',
            width: 160
        },
        {
            field: 'usuario',
            headerName: 'Usuario',
            width: 160
        },
        {
            field: 'area',
            headerName: 'Área',
            width: 160
        },
        {
            field: 'saldo_capital',
            headerName: 'Saldo capital',
            width: 160
        },
        {
            field: 'capital',
            headerName: 'Capital',
            width: 160
        },
        {
            field: 'capital_vencido',
            headerName: 'Capital vencido',
            width: 160
        },
        {
            field: 'interes',
            headerName: 'Interés',
            width: 160
        },
        {
            field: 'interes_moratorio',
            headerName: 'Interés moratorio',
            width: 160
        },
        {
            field: 'cuotas_de_manejo',
            headerName: 'Cuotas de manejo',
            width: 160
        },
        {
            field: 'seguros',
            headerName: 'Seguros',
            width: 160
        },
        {
            field: 'gastos_de_cobranza',
            headerName: 'Gastos de cobranza',
            width: 160
        }
    ]
    consultaCaller.Call(columns, filter, DataAPIService.getCambioEstado)
}