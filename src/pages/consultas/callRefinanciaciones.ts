import {FilterData} from "../../model/IfilterData";
import {ConsultaCaller, MidasCols} from "./call";
import {DataAPIService} from "../../services/data-api.service";

export function callRefinanciaciones(filter: FilterData, consultaCaller: ConsultaCaller) {
    const columns: MidasCols[] =  [
        {
            field: 'fecha_negociacion',
            headerName: 'Fecha de negociación',
            type: 'string',
            width:160,
        },
        {
            field: 'fecha_aplicacion',
            headerName: 'Fecha de aplicación',
            type: 'string',
            width:152,
        },
        {
            field: 'numero_compra_modificado',
            headerName: 'Número de compra modificado',
            type: 'number',
            width:216,
        },
        {
            field: 'tasa_interes_actual',
            headerName: 'Tasa de interés actual',
            type: 'number',
            width:160,
        },
        {
            field: 'tasa_interes_anterior',
            headerName: 'Tasa de interés anterior',
            type: 'number',
            width:170,
        },
        {
            field: 'valor_cuota',
            headerName: 'Valor de cuota',
            type: 'money',
            width:115,
        },
        {
            field: 'valor_cuota_anterior',
            headerName: 'Valor de cuota anterior ',
            type: 'money',
            width:165,
        },
        {
            field: 'valor_cuota_nueva',
            headerName: 'Valor de cuota nueva',
            type: 'money',
            width:160,
        },
        {
            field: 'plazo_anterior',
            headerName: 'Plazo anterior',
            type: 'string',
            width:110 ,
        },

        {
            field: 'nuevo_plazo',
            headerName: 'Nuevo plazo',
            type: 'string',
            width:100,
        },
        {
            field: 'tipo_modificacion',
            headerName: 'Tipo de modificación',
            type: 'string',
            width:152,
        },
        {
            field: 'estado_negociacion',
            headerName: 'Estado negociación',
            type: 'string',
            width:145,
        },
        {
            field: 'subproducto',
            headerName: 'Subproducto',
            type: 'string',
            width:105,
        },
        {
            field: 'id_negociacion',
            headerName: 'ID de negociación',
            type: 'string',
            width:136,
        },
        {
            field: 'capital_vencido',
            headerName: 'Capital vencido',
            type: 'money',
            width:120,
        },
        {
            field: 'saldo_capital',
            headerName: 'Saldo capital',
            type: 'money',
            width:105,
        },
        {
            field: 'rol_modificado',
            headerName: 'Usuario o rol de modificación',
            type: 'string',
            width:210,
        },
        {
            field: 'area_modificacion',
            headerName: 'Área de modificación',
            type: 'string',
            // flex: 1,
            width:155,
        },
    ];
    consultaCaller.Call(columns,filter,DataAPIService.getRefinanciaciones)
}