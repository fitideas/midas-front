import { FilterData } from "../../model/IfilterData";
import { DataAPIService } from "../../services/data-api.service";
import { ConsultaCaller, MidasCols } from "./call";

export function callTraslados(filter: FilterData, consultaCaller: ConsultaCaller) {
    const columns: MidasCols[] = [
        {
            field: 'subproducto',
            headerName: 'Subproducto',
            width: 125,
        },
        {
            field: 'numero_identificacion',
            headerName: 'Número de identificación',
            type: 'number',
            width: 180,
        },        
        {
            field: 'cuenta_anterior',
            headerName: 'Cuenta anterior',
            type: 'number',
            width: 140,
        },
        {
            field: 'cuenta_actual',
            headerName: 'Cuenta actual',
            type: 'number',
            width: 140,
        },
        {
            field: 'fecha_traslado',
            headerName: 'Fecha de traslados',
            type: 'string',
            width: 160,

        },
        {
            field: 'usuario',
            headerName: 'Usuario',
            type: 'number',
            width: 100,

        },
        {
            field: 'ciclo_anterior',
            headerName: 'Ciclo anterior',
            type: 'number',
            width: 160,
        },
        {
            field: 'ciclo_nuevo',
            headerName: 'Ciclo nuevo',
            type: 'number',
            width: 140,
        },
        {
            field: 'numero_cuotas_facturadas',
            headerName: 'Numero de cuotas facturadas',
            type: 'number',
            width: 140,
        },
        {
            field: 'numero_cuotas_actual',
            headerName: 'Numero de cuotas actual',
            type: 'number',
            width: 140,
        },
        {
            field: 'capital',
            headerName: 'Capital',
            type: 'number',
            width: 140,
        },
        {
            field: 'capital_vencido',
            headerName: 'Capital vencido',
            type: 'number',
            width: 140,
        },
        {
            field: 'saldo_capital',
            headerName: 'Saldo capital',
            type: 'number',
            width: 140,
        },
        {
            field: 'interes_corriente',
            headerName: 'Interes corriente',
            type: 'number',
            width: 140,
        },
        {
            field: 'interes_moratorio',
            headerName: 'Interes moratorio',
            type: 'number',
            width: 140,
        },
        {
            field: 'gastos_cobranza',
            headerName: 'Gastos de cobranza',
            type: 'number',
            width: 140,
        },
        {
            field: 'cuota_manejo',
            headerName: 'Cuota de manejo',
            type: 'number',
            width: 140,
        },
        {
            field: 'seguros',
            headerName: 'Seguros',
            type: 'number',
            width: 140,
        }
    ];
    consultaCaller.Call(columns,filter,DataAPIService.getTraslados)
}