/**
 * Cargue Reclamaciones Page
 */
import React from "react";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import {useSnackbar} from "notistack";
import CircularProgress from "@mui/material/CircularProgress";
import {DataAPIService} from "../../services/data-api.service";

const CargueReclamaciones = (): JSX.Element => {
    /**
     * File React state
     */
    const [file, setFile] = React.useState<File | null>(null);
    /**
     * Loading React State
     */
    const [loading, setLoading] = React.useState<boolean>(false);
    /**
     * uploadResult React State
     */
    const [uploadResult, setUploadResult] = React.useState<any | null>(null);
    /**
     * Notification Stack Context
     */
    const snackbarContext = useSnackbar();
    /**
     * On File Change handler
     * @param event Change Event
     */
    const onChangeFile = (event: React.ChangeEvent<HTMLInputElement>) => {
        let fileList: FileList | null = event.target.files;
        if(fileList != null){
            setFile(fileList[0]);
            snackbarContext.enqueueSnackbar('Archivo cargado exitosamente.', {variant: 'success'});
        }else{
            setFile(null);
        }
        setUploadResult(null);
    }
    /**
     * onConfirm delete button handler
     */
    const onDelete = () => {
        if(file != null){
            setLoading(true);
            DataAPIService.uploadReclamaciones(file).then(response => {
                if (response.data.data !=null){
                    setUploadResult(response.data.data);
                }
                setLoading(false);
            } ).catch(reason => {
                console.log(reason);
                setLoading(false);
                snackbarContext.enqueueSnackbar('Ha ocurrido un error procesando la información.', {variant: 'error'});
            })
        }
    };
    return (
        <Grid container justifyContent="center" alignContent="center" padding="4rem" textAlign="center" spacing={2}>
            <Grid item xs={12}>
                <Typography variant="h4" gutterBottom component="div">
                    Cargue Información reclamaciones en curso
                </Typography>
            </Grid>
            <Grid item xs={12}>
                <Button variant="contained" component="label">
                    Subir archivo
                    <input accept=".xlsx" type="file" hidden={true} onChange={onChangeFile}/>
                </Button>
            </Grid>
            <Grid item xs={12}>
                <Typography variant="h6" gutterBottom component="div">
                    {file == null ? "" : "Archivo cargado: \"" + file.name + "\""}
                </Typography>
            </Grid>
            <Grid item  xs={12} textAlign="center">
                {file != null && (
                    <Button variant="contained" component="label" onClick={onDelete} disabled={loading || uploadResult != null}>
                        Subir Reclamaciones
                    </Button>
                )}
            </Grid>
            <Grid item xs={12}>
                {loading && <CircularProgress  variant={'indeterminate'}/>}
            </Grid>
            <Grid item xs={12}>
                {loading && "Procesando archivo"}
            </Grid>
            {uploadResult != null&&(
                <Grid item container xs={3}  alignContent={"center"} justifyContent={"center"} >
                    <Grid item xs={12}>
                        <Typography variant="h5" gutterBottom component="div">
                            Resultado de la operación
                        </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography variant="h6" gutterBottom component="div">
                            Carga exitosa
                        </Typography>
                    </Grid>
                    <Grid item xs={6}>
                        Registros Procesados
                    </Grid>
                    <Grid item xs={3}>
                        {uploadResult}
                    </Grid>
                </Grid>
            )}
        </Grid>
    );
}
export default CargueReclamaciones;