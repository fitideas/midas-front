/**
 * Casas de cobranza page
 */

// React
import React from "react";

// Material design
import Button from "@mui/material/Button";
import {Backdrop, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle} from "@mui/material";
import CircularProgress from "@mui/material/CircularProgress";

// Notistack
import {ProviderContext, useSnackbar} from "notistack";

// Axios
import {AxiosError, AxiosResponse} from "axios";

// Midas
import {DataAPIService} from "../../services/data-api.service";
import {NavigateFunction, useNavigate} from "react-router-dom";



/**
 * Generar carta a morosos React Component
 * @constructor
 */
const CasasDeCobranza = (): JSX.Element => {

    /**
     * Last load State
     */
    const [lastLoad, setLastLoad]: [string, React.Dispatch<React.SetStateAction<string>>] = React.useState<string>("");

    /**
     * Generating State
     */
    const [generating, setGenerating]: [boolean, React.Dispatch<React.SetStateAction<boolean>>] = React.useState<boolean>(false);

    /**
     * Notification Stack Context
     */
    const snackbarContext: ProviderContext = useSnackbar();

    /**
     * Navigator Context
     */
    const navigator: NavigateFunction = useNavigate();

    /**
     * On click close dialog
     * Handle the exit dialog
     */
    const onClose = (): void => {
        snackbarContext.enqueueSnackbar('Generación cancelada', {variant: 'warning'});
        navigator(-1);
    }

    /**
     * On click generate button
     * Handle the generation
     */
    const onGenerate = (): void => {
        setGenerating(true);
        DataAPIService.generarCasasDeCobranza()
            .then((response: AxiosResponse): void => {
                snackbarContext.enqueueSnackbar(response.data.message, {variant: 'success'});
            })
            .catch((error: AxiosError): void => {
                snackbarContext.enqueueSnackbar(error.response?.data.message, {variant: 'error'});
            }).finally((): void => {
                setGenerating(false);
                navigator(-1);
            })
    }

    const getLast = (): void => {
        DataAPIService.getLastFechaCartera()
            .then((response: AxiosResponse): void => {
                setLastLoad(response.data.data);
            })
            .catch((error: AxiosError): void => {
                snackbarContext.enqueueSnackbar(error.response?.data.message, {variant: 'error'});
            })
    }
    React.useEffect(getLast, []);

    return (
        <div>
            <Dialog onClose={onClose} open={!generating}>
                <DialogTitle>¿Generar información de cartera para casas de cobranza?</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        La última carga de cartera se hizo el {lastLoad} ¿Desea generar la información?
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={onClose}>
                        Cancelar
                    </Button>
                    <Button onClick={onGenerate}>
                        Generar
                    </Button>
                </DialogActions>
            </Dialog>
            <Backdrop
                sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
                open={generating}
            >
                <CircularProgress />
            </Backdrop>
        </div>
    );
}

export default CasasDeCobranza;