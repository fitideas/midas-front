/**
 * Generar carta a morosos Page
 */

// React
import React from "react";

// Material design
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import {CircularProgress, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Grow} from "@mui/material";

// Iconify
import {Icon} from "@iconify/react";

// Notistack
import {ProviderContext, useSnackbar} from "notistack";

// Axios
import {AxiosError, AxiosResponse} from "axios";

// Midas
import {DataAPIService} from "../../services/data-api.service";
import './style.css';

    export interface DownloadDialogProps {
        open: boolean;
        onClose: (reason:string) => void;
        onCloseDownload: (value: string) => void;
    }

  function DownloadDialog(props: DownloadDialogProps) {
    const { onClose, onCloseDownload, open} = props;
  
    const handleClose = (reason: string) => {
      onClose(reason);
    };

    const selectExt = (value: string) => {
        onCloseDownload(value);
    };
  
    return (
      <Dialog onClose={handleClose} open={open}>
        <DialogTitle>Descarga</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Confirmar la descarga del Segundo Preaviso
          </DialogContentText>
        </DialogContent>
        <span className="dialog-buttons">
            <Button variant="contained" sx={{margin:1}} onClick={() => selectExt('xlsx')}>EXCEL</Button>
            <Button variant="contained" sx={{margin:1}} onClick={() => selectExt('txt')}>TXT</Button>
        </span>
      </Dialog>
    );
  }

/**
 * Generar carta a morosos React Component
 * @constructor
 */
const GenerarSegundoPreaviso = (): JSX.Element => {

    
    const [open, setOpen] = React.useState(false);
    const [openProgress, setOpenProgress] = React.useState(false);
  
    const handleClickOpen = () => {
      setOpen(true);
    };

    const closeProgressDialog = (reason: string) => {
        if (reason === "backdropClick"){
            return;
        }
    };

    const onCloseHandle = (reason: string) => {
        if (reason === "backdropClick"){
            return;
        }
    };

     /**
      * Notification Stack Context
      */
     const snackbarContext: ProviderContext = useSnackbar();
  
    const closeDownloadDialog = (value: string) => {
        setOpen(false);
        if(value !== ""){
            setOpenProgress(true);
            DataAPIService.generarSegundoPreaviso(value === "xlsx" ? 1 : 0).then((response: AxiosResponse): void => {
                if (response.status === 204) {
                    setOpenProgress(false);
                    snackbarContext.enqueueSnackbar('No hay registros para generar Segundo Preaviso.', {variant: 'warning'});
                } else {
                    let today: Date = new Date(Date.now());
                    let filename: String = "Segundo_Preaviso" + '_' + today.getFullYear() + String(today.getMonth() + 1).padStart(2, '0') + String(today.getDate()).padStart(2, '0');;
                    const url = window.URL.createObjectURL(new Blob([response.data]));
                    const link = document.createElement('a');
                    link.href = url;
                    link.setAttribute('download', filename + "." + value);
                    document.body.appendChild(link);
                    link.click();
                    setOpenProgress(false);
                }
            }).catch((error: AxiosError): void => {
                setOpenProgress(false);
                snackbarContext.enqueueSnackbar('Ha ocurrido un error consultando la información.', {variant: 'error'});
                console.error(error);
            });

        }else{
            setOpenProgress(true);
        }
    };

    return (
        <Grid container spacing={2} className={"container"}>
            <Grid item xs={12}>
                <Typography variant="h4" gutterBottom component="div">
                    Generación de Segundo Preaviso
                </Typography>
            </Grid>
            <Grid item xs={12}>
                <div>
                    <Button variant="contained" onClick={() => handleClickOpen()}>Generar</Button>
                    <DownloadDialog open={open} onClose={onCloseHandle} onCloseDownload={closeDownloadDialog}/>
                    <Dialog onClose={closeProgressDialog} open={openProgress} disableEscapeKeyDown={false}>
                        <DialogContent>
                            <CircularProgress/>
                        </DialogContent>
                    </Dialog>
                </div>
            </Grid>
        </Grid>
    );
}

export default GenerarSegundoPreaviso;