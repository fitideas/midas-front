/**
 * Calendario Facturacion Page
 */

// React
import React from "react";

// Material Design
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import CircularProgress from '@mui/material/CircularProgress';
import Grid from "@mui/material/Grid";
import TextField from "@mui/material/TextField";
import FormControl from "@mui/material/FormControl";

// Notistack
import {ProviderContext, useSnackbar} from "notistack";

// Axios
import {AxiosError, AxiosResponse} from "axios";

// Midas
import {DataAPIService} from "../../services/data-api.service";
import './style.css';


/**
 * Calendario Facturacion React Component
 * @constructor
 */
const CalendarioFacturacion = (): JSX.Element => {

    /**
     * Last Version state
     */
    const [lastVersion, setLastVersion]: [string, React.Dispatch<React.SetStateAction<string>>] = React.useState<string>("");

    /**
     * File React state
     */
    const [file, setFile]: [File | null , React.Dispatch<React.SetStateAction<File | null>>] = React.useState<File | null>(null);

    /**
     * Version React state
     */
    const [version, setVersion]: [string|null, React.Dispatch<React.SetStateAction<string|null>>] = React.useState<string|null>(null);

    /**
     * Loading React State
     */
    const [loading, setLoading]: [boolean, React.Dispatch<React.SetStateAction<boolean>>] = React.useState<boolean>(false);

    /**
     * Loading React State
     */
    const [uploadProgress, setUploadProgress]: [number, React.Dispatch<React.SetStateAction<number>>] = React.useState<number>(0.0);

    /**
     * Loading React State
     */
    const [uploading, setUploading]: [boolean, React.Dispatch<React.SetStateAction<boolean>>] = React.useState<boolean>(false);

    /**
     * Notification Stack Context
     */
    const snackbarContext: ProviderContext = useSnackbar();

    /**
     * On click Upload Button.
     * Handle the upload of file version into backend
     */
    const onClickUpload = (): void => {

        /**
         * On Upload Progress callback
         * @param event Progress event
         */
        const onProgress: (event: ProgressEvent) => void = (event: ProgressEvent) => {
            let p: number = event.loaded / event.total * 100;
            setUploadProgress(p);
            if(p == 100){
                setUploading(false);
            }
        };

        if(file == null){
            snackbarContext.enqueueSnackbar('Ingrese por favor un archivo.', {variant: 'error'});
        }else if(version === ""||version==null||(!/^(\d{4}_(.+))$/.test(version as string))) {
            if(!/^(\d{4}_(.+))$/.test(version as string))setVersion("");
            snackbarContext.enqueueSnackbar('Ingrese por favor una versión valida.', {variant: 'error'});
        }else {
            DataAPIService.isVersionNoRepeated(version)
                .then((response: AxiosResponse): void => {
                    if(response.data.data){
                        setUploading(true);
                        setLoading(true);
                        DataAPIService.uploadCalendarioFacturacion(file, version, onProgress)
                            .then((response: AxiosResponse) => {
                                console.log("Response2: ", response);
                                snackbarContext.enqueueSnackbar('La información se ha procesado correctamente.', {variant: 'success'});
                                getLast();
                            }).catch((error: AxiosError) => {
                                console.log(error.message);
                                snackbarContext.enqueueSnackbar('Ha ocurrido un error procesando la información.', {variant: 'error'});
                            }).finally(() =>{
                                setLoading(false);
                                setUploading(false);
                            });
                    }else{
                        snackbarContext.enqueueSnackbar('La versión indicada ya fue cargada anteriormente.', {variant: 'error'});
                    }
                })
                .catch((error: AxiosError): void => {
                    console.error(error.message);
                    snackbarContext.enqueueSnackbar('Ha ocurrido un error consultando la información.', {variant: 'error'});
                    setLoading(false);
                });
        }
    }

    /**
     * On File Change handler
     * @param event Change Event
     */
    const onChangeFile = (event: React.ChangeEvent<HTMLInputElement>) => {
        let fileList: FileList | null = event.target.files;
        if(fileList != null && fileList.length > 0){
            setFile(fileList[0]);
            snackbarContext.enqueueSnackbar('Archivo cargado exitosamente.', {variant: 'success'});
        }else{
            setFile(null);
        }
    }
    const getLast=() =>{
        DataAPIService.getLastCalendarioFacturacion()
            .then((response: AxiosResponse): void => {
                console.log("Response Last Version: ", response.data.data);
                setLastVersion(response.data.data);
            });}
    React.useEffect(getLast, []);

    return (
        <Grid container spacing={2} className={"container"}>
            <Grid item xs={12}>
                <Typography variant="h4" gutterBottom component="div">
                    Credito facil codensa
                </Typography>
            </Grid>
            <Grid item xs={12}>
                <Typography variant="h6" gutterBottom component="div">
                    Última versión cargada: {lastVersion}
                </Typography>
            </Grid>
            <Grid item xs={12}>
                <FormControl>
                    <Grid container spacing={0}>
                        <Grid item xs={7}>
                            <TextField
                                onChange={(event: React.ChangeEvent<HTMLInputElement>) => {setVersion(event.target.value)}}
                                value={version}
                                label="Versión (AAAA_Versión)"
                                variant="outlined"
                                error={(!/^(\d{4}_(.+))$/.test(version as string))&&version!==null}
                            />
                        </Grid>
                        <Grid item xs={5}  className={"upload-button"}>
                            <Button variant="contained" component="label">
                                Subir archivo
                                <input accept=".xlsx, .csv" id="contained-button-file" type="file" hidden={true} onChange={onChangeFile}/>
                            </Button>
                        </Grid>
                        <Grid item xs={12}>
                            <Typography variant="h6" gutterBottom component="div">
                                {file == null ? "" : "Archivo cargado: \"" + file.name + "\""}
                            </Typography>
                        </Grid>
                        <Grid item xs={12} className={"process-button"}>
                            <Button variant="contained" onClick={onClickUpload} centerRipple={true}>
                                Procesar archivo
                            </Button>
                        </Grid>
                    </Grid>
                </FormControl>
            </Grid>
            <Grid item xs={12} className={"loading-section"}>
                {loading && <CircularProgress value={uploadProgress} variant={uploading ? 'determinate' : 'indeterminate'}/>}
            </Grid>
            <Grid item xs={12}>
                {uploading && "Cargando archivo"}
                {loading && !uploading && "Procesando archivo"}
            </Grid>
        </Grid>
    );
};

export default CalendarioFacturacion;
