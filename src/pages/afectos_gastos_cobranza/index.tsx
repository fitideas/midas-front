/**
 * Afectos Gastos Cobranza Page
 */
import React from "react";
import {Grid} from "@mui/material";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import {DataAPIService} from "../../services/data-api.service";
import {useSnackbar} from "notistack";
import CircularProgress from "@mui/material/CircularProgress";

const AfectosGastosCobranza = (): JSX.Element => {
    /**
     * Entries React State
     */
    const [entries, setEntries] = React.useState<number|null>(null);
    /**
     * Loading React State
     */
    const [loading, setLoading] = React.useState<boolean>(false);
    /**
     * Notification Stack Context
     */
    const snackbarContext = useSnackbar();
    /**
     * Retrieve amount of Afectos at first rendering
     */
    const  generateAfectos = ()=>{
        setEntries(null);
        setLoading(true);
        DataAPIService.getNumberAfectos().then(response => {
            setEntries(response.data.data);
            setLoading(false);
        } ).catch(reason => {
            console.log(reason);
            setLoading(false);
            snackbarContext.enqueueSnackbar('Ha ocurrido un error durante la generación de información.', {variant: 'error'});
        })
    }
    return (
        <Grid container justifyContent="center" alignContent="center" padding="4rem" textAlign="center" spacing={2}>
            <Grid item xs={12}>
                <Typography variant="h4" gutterBottom component="div">
                    Afectos Gastos de Cobranza
                </Typography>
            </Grid>
            <Grid item xs={12}>
                <Button variant="contained" component="label" onClick={generateAfectos} disabled={loading}>
                    Generar Afectos de gastos de cobranza
                </Button>
            </Grid>
            {entries!=null&&(
                <Grid item container xs={3}>
                    <Grid item xs={12}>
                        <Typography variant="h6" gutterBottom component="div">
                            Numero de registros generados: {entries}
                        </Typography>
                    </Grid>
                </Grid>
            )}
            {loading&&(
                <Grid item xs={12}>
                    {loading && <CircularProgress  variant={'indeterminate'}/>}
                </Grid>
            )}
        </Grid>
    )
}
export default AfectosGastosCobranza