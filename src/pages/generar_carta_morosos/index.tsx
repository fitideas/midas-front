/**
 * Generar carta a morosos Page
 */

// React
import React from "react";

// Material design
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import FormControl from "@mui/material/FormControl";
import TextField from "@mui/material/TextField";
import MenuItem from '@mui/material/MenuItem';
import Chip from '@mui/material/Chip';
import Select, {SelectChangeEvent} from '@mui/material/Select';
import OutlinedInput from '@mui/material/OutlinedInput';
import Button from "@mui/material/Button";
import InputLabel from "@mui/material/InputLabel";

// Notistack
import {ProviderContext, useSnackbar} from "notistack";

// Axios
import {AxiosError, AxiosResponse} from "axios";

// Midas
import {DataAPIService} from "../../services/data-api.service";
import './style.css';


/**
 * Generar carta a morosos React Component
 * @constructor
 */
const GenerarCartaMorosos = (): JSX.Element => {

    /**
     * Status State
     */
    const [statusList, setStatusList]: [string[], React.Dispatch<React.SetStateAction<string[]>>] = React.useState<string[]>(["SI FACTURA", "NO FACTURA", "CASTIGADOS"]);

    /**
     * Facturation Cycle State
     */
    const [billingCycle, setBillingCycle]: [number, React.Dispatch<React.SetStateAction<number>>] = React.useState<number>(1);

    /**
     * Notification Stack Context
     */
    const snackbarContext: ProviderContext = useSnackbar();

    /**
     * On click Upload Button.
     * Handle the generation of report
     */
    const onClickUpload = (ext: string): void => {
        DataAPIService.generateCartaMorosos(statusList, billingCycle, ext == "xlsx" ? 1 : 0)
            .then((response: AxiosResponse): void => {
                if (response.status == 204) {
                    snackbarContext.enqueueSnackbar('No hay cartas para generar.', {variant: 'warning'});
                } else {
                    let today: Date = new Date(Date.now());
                    let filename: String = "carta_morosa" + '_' + today.getFullYear() + String(today.getMonth() + 1).padStart(2, '0') + String(today.getDate()).padStart(2, '0');
                    const url = window.URL.createObjectURL(new Blob([response.data]));
                    const link = document.createElement('a');
                    link.href = url;
                    link.setAttribute('download', filename + "." + ext);
                    document.body.appendChild(link);
                    link.click();
                }
            })
            .catch((error: AxiosError): void => {
                if(error.response?.headers["content-type"] == "application/json") {
                    const fr: FileReader = new FileReader();
                    fr.onload = (e: ProgressEvent<FileReader>) => {
                        if (typeof e.target?.result === "string") {
                            snackbarContext.enqueueSnackbar(JSON.parse(e.target.result)["message"], {variant: 'error'});
                        }
                    }
                    fr.readAsText(error.response.data);
                }
            });
    }

    return (
        <Grid container spacing={2} className={"container"}>
            <Grid item xs={12}>
                <Typography variant="h4" gutterBottom component="div">
                    Generación de carta a morosos
                </Typography>
            </Grid>
            <Grid item xs={12}>
                <FormControl sx={{ m: 1, minWidth: 375 }}>
                    <InputLabel id="tipo-doc-select-label">Estados</InputLabel>
                    <Select
                        labelId="tipo-doc-select-label"
                        value={statusList}
                        multiple
                        className={"form-item"}
                        label={"Estados"}
                        onChange={(event: SelectChangeEvent<typeof statusList>) => {
                            const {
                                target: {value},
                            } = event;
                            setStatusList(typeof value === 'string' ? value.split(',') : value);
                        }}
                        input={<OutlinedInput id="select-multiple-chip" label="Estados" />}
                        renderValue={(rendered: string[]) => (
                            rendered.map((element: string) => (
                                    <Chip label={element}/>
                                )
                            )
                        )}
                    >
                        <MenuItem value={"SI FACTURA"}>
                            SI FACTURA
                        </MenuItem>
                        <MenuItem value={"NO FACTURA"}>
                            NO FACTURA
                        </MenuItem>
                        <MenuItem value={"CASTIGADOS"}>
                            CASTIGADOS
                        </MenuItem>
                    </Select>
                    <TextField
                        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                            setBillingCycle(parseInt(event.target.value));
                        }}
                        className={"form-item"}
                        value={billingCycle}
                        InputProps={{inputProps: {type: 'number', min: 1}}}
                        label="Ciclo de facturación"
                        variant="outlined"
                    />

                    <Button
                        variant={"contained"}
                        onClick={
                            () => {
                                onClickUpload("xlsx");
                            }
                        }>
                        Generar
                    </Button>
                </FormControl>
            </Grid>
        </Grid>
    );
}

export default GenerarCartaMorosos;