import { Typography } from '@mui/material';
import image from '../../assets/home.jpg';
import './style.css';

const HomePage = () => {
  return (
    <div className="img-with-text">
      <img src={image} alt="John Doe" />
      <br/>
      <Typography variant="h4" gutterBottom component="div">
        Credito facil codensa
      </Typography>
    </div>
  );
};

export default HomePage;
