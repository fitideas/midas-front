import { createTheme } from "@mui/material";

const mainTheme = createTheme({
    palette: {
      primary: {
        main: '#662d83',
      },
      common:{
          white:'white'
      }
    },
  });

  export default mainTheme;